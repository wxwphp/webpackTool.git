var path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
  var config = {
//入口文件
    entry: {
        index : './src/js/common.js'
    },
    //出口文件
    output: {
        path: path.resolve(__dirname,"dist"),    //编译后的文件路径
        filename: 'js/app.js',
    },
    module: {
        //Loaders
        rules: [
            //.css 文件使用 style-loader 和 css-loader 来处理
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            //图片文件使用 url-loader 来处理，小于8kb的直接转为base64
            {
                test: /\.(png|jpg)$/,
                loader: 'url-loader',
                options:{
                    limit:5000,
                    name:'image/[name].[hash:8].[ext]'
                },
            },
            {
                test: /\.html$/,
                use:[
                    {
                        loader:'html-withimg-loader'
                    },
                    {
                        loader:'html-loader',
                        query:'require'
                    }
                ]
            }
        ],

    },
    plugins:[
        new HtmlWebpackPlugin({
            template:'./index.html',
            filename:'index.html',
            inject:"head"
        })
    ]
};

module.exports = config;
